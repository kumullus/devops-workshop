# Outils à installer pour l'atelier
Salut voici une liste exhaustive des choses à installer pour l'atelier de demain, si vous avez besoin n'hésitez pas à me demander quoi que ce soit !

## AWS-cli

Dans un terminal , tapper :
```
$ pip install --user aws-cli
```

Une fois l'outil installé, vous devez configurer votre compte en utilisant la commande:
```
$ aws configure
```

avec :

-   AWS_ACCESS_KEY_ID: votre access key aws
-   AWS_SECRET_ACCESS_KEY: votre secret key aws
-   Default region name: **_eu-west-3_**
-   Default output format: **_json_**

Pour récupérer votre access / secret key il faut vous connecter à AWS en allant à l'adresse suivante : [https://kumullus.signin.aws.amazon.com/console](https://kumullus.signin.aws.amazon.com/console)

(Si vous avez oublié vos identifiants, dites le moi ;) ), puis :

_Cliquez sur votre nom en haut à droite > "Mes informations d'identifications ..." > Utilisateurs > votre_nom > "Informations d'identification ..." > Créer une clé d'accès_

N'oubliez pas de télécharger le fichier qu'AWS vous propose de télécharger et qui contient vos clés

## Docker

Premièrement, si vous ne l'avez pas encore, commencez par télécharger Docker, soit en vous rendant sur:

-   [https://www.docker.com/community-edition](https://www.docker.com/community-edition)
-   soit en tappant les commandes:

```
$ sudo apt-get update

$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

$ sudo apt-get update

$ sudo apt-get install docker-ce
```

Pour tester que l'installation s'est bien passée vous pouvez run la commande:

```
$ docker run hello-world
```

Qui devrait vous afficher un message similaire :


```
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/engine/userguide/
```

Il faudra aussi vous créer un compte sur Docker Hub (registry docker par défaut) : [https://hub.docker.com/](https://hub.docker.com/)

## Ansible

Pour installer Ansible, une seule commande est nécessaire:

```
$ sudo pip install --user ansible
```

## Terraform + Packer

Pour installer les outils Terraform et Packer, il faut tout d'abord télécharger les ZIPs depuis ces deux URLS:

-   Terraform : [https://www.terraform.io/downloads.html](https://www.terraform.io/downloads.html)
-   Packer: [https://www.packer.io/downloads.html](https://www.packer.io/downloads.html)

Une fois téléchargé, vous pouvez dézipper et installer les deux fichiers en utilisant les commandes suivantes:

```
$ cd terraform_download_path
$ unzip terraform_version.zip -d /usr/local/bin
$ unzip packer_version.zip -d /usr/local/bin
```

Une fois ces outils installés vous pouvez vérifier l'installation en tappant les commandes:

```
$ terraform version
$ packer version
```

### Et voilà ! All SET pour l'atelier !

# Documentation partagée

Documentation écrite au cours de l'atelier : [lien](https://docs.google.com/document/d/1s5UdMiM-HyQEjSKLNA5gJA1RpEBs6IuhmYGcz4GIjiQ/edit?ts=5b695c8a#heading=h.r2ykzmxfm5xg)
