provider "aws" {
  region = "eu-west-3"
}

variable "packer_ami" {}
variable "cloudfront_alias" {}

resource "aws_default_vpc" "default_vpc" {}

# Creating Redis instances
resource "aws_elasticache_cluster" "workshop-cluster" {
  cluster_id = "tf-workshop-cluster"
  engine = "redis"
  node_type = "cache.t2.micro"
  num_cache_nodes = 1
}

# EC2 Instance for the API

resource "aws_instance" "workshop-api" {
  ami = "${var.packer_ami}"
  instance_type = "t2.micro"

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name = "My First Workshop Instance !"
  }
}

resource "aws_eip" "api_eip" {
  instance = "${aws_instance.workshop-api.id}"
  vpc      = true
}

# S3 + Cloudfront for the frontend
resource "aws_s3_bucket" "workshop_front_bucket" {
  bucket = "${var.cloudfront_alias}"
  acl = "public-read"

  versioning {
    enabled = false
  }

  website {
    index_document = "index.html"
  }

  tags {
    Name = "Kumullus workshop staging frontend"
    Environment = "production"
  }
}


resource "aws_cloudfront_distribution" "workshop_distrib" {
  origin {
    domain_name = "${aws_s3_bucket.workshop_front_bucket.bucket_domain_name}"
    origin_id = "workshopFrontStagingS3Origin"
  }

  aliases = ["${var.cloudfront_alias}"]

  enabled = true
  is_ipv6_enabled = true
  comment = "Worshop distribution"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods = ["GET", "HEAD"]
    target_origin_id = "workshopFrontStagingS3Origin"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl  = 0
    default_ttl = 3600
    max_ttl = 86400
  }

  price_class = "PriceClass_100"

  tags {
    Environment = "staging"
  }

  viewer_certificate {
    cloudfront_default_certificate = true
    minimum_protocol_version = "TLSv1.1_2016"
    ssl_support_method = "sni-only"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}
