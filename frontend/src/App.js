import React, { Component } from "react";
import logo from "./logo.svg";
import {
  getServerHost,
  getServerInfos,
  changeCounterValue,
  getCounterValue
} from "./service";
import "bulma";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { infos: { ipList: [] }, counter: 0 };
  }
  componentDidMount(props) {
    Promise.all([getServerHost(), getServerInfos(), getCounterValue()]).then(
      ([host, infos, counter]) => {
        this.setState({
          host: host.hostname,
          infos,
          counter: counter.counter
        });
      }
    );
  }

  renderCard(title, ips) {
    return (
      <div className="card column" key={ips[0].value}>
        <div className="card-content">
          <div className="title">{title}</div>
          <div className="subtitle">
            {ips.map(ip => (
              <div key={ip.type + ip.value} style={{ padding: "10px" }}>
                {ip.type}: {ip.value}
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }

  renderInfos() {
    const { infos: { ipList } } = this.state;
    return ipList.length > 0 ? (
      ipList.map(([ipv4 = { address: "" }, ipv6 = { address: "" }]) =>
        this.renderCard(ipv4.interface, [
          { type: "IPv4", value: ipv4.address },
          { type: "IPv6", value: ipv6.address }
        ])
      )
    ) : (
      <h3>No Datas found</h3>
    );
  }

  handleClick = async value => {
    const result = await changeCounterValue(value);
    this.setState({ counter: result.new });
  };

  render() {
    return (
      <div className="App">
        <section className="hero is-primary">
          <div className="hero-body">
            <div className="container">
              <h1 className="title">{this.state.host}</h1>
            </div>
          </div>
        </section>
        <div className="columns">{this.renderInfos()}</div>
        <div>
          <div className="buttons has-addons">
            <span className="button" onClick={() => this.handleClick(1)}>
              +1
            </span>
            <span className="button" disabled>
              {this.state.counter}
            </span>
            <span
              className="button is-danger is-selected"
              onClick={() => this.handleClick(-1)}
            >
              -1
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
