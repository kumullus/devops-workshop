import axios from "axios";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API
});

export async function getServerHost() {
  const res = await axiosInstance.get("/host");
  return res.data;
}

export async function getServerInfos() {
  const res = await axiosInstance.get("/info");
  return res.data;
}

export async function getCounterValue() {
  const res = await axiosInstance.get("/counter");
  return res.data;
}

export async function changeCounterValue(increment) {
  const datas = JSON.stringify({
    increment
  });
  const res = await axiosInstance.post("/counter", datas, {
    headers: { "Content-Type": "application/json" }
  });

  return res.data;
}
