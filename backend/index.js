"use strict";
// ttest
require("dotenv").config();
const express = require("express");
const os = require("os");
const ifaces = os.networkInterfaces();
const cors = require("cors");
const morgan = require("morgan");
const redis = require("redis");
const bodyParser = require("body-parser");

function extractIP() {
  return Object.keys(ifaces).map(ifname => {
    let alias = 0;
    return ifaces[ifname].map(iface => {
      return {
        interface: `${ifname}${alias === 0 ? "" : ":" + alias}`,
        address: iface.address
      };
      ++alias;
    });
  });
}

const { REDIS_HOST, REDIS_PORT } = process.env;

const client = redis.createClient({
  host: REDIS_HOST,
  port: REDIS_PORT,
  retry_strategy: function(options) {
    if (options.error && options.error.code === "ECONNREFUSED") {
      // End reconnecting on a specific error and flush all commands with
      // a individual error
      return new Error("The server refused the connection");
    }
    if (options.total_retry_time > 1000 * 60 * 60) {
      // End reconnecting after a specific timeout and flush all commands
      // with a individual error
      return new Error("Retry time exhausted");
    }
    if (options.attempt > 10) {
      // End reconnecting with built in error
      return undefined;
    }
    // reconnect after
    return Math.min(options.attempt * 100, 3000);
  }
});

client.on("error", err => {
  console.error(err);
});

client.on("ready", () => {
  const app = express();

  app.use(morgan("tiny"));

  app.use(cors());

  app.use(
    bodyParser.urlencoded({
      extented: true
    })
  );
  app.use(bodyParser.json());

  app.get("/", (req, res) => {
    res.send("Hello World !");
  });

  app.get("/info", (req, res) => {
    const ipList = extractIP();
    res.status(200).json({ ipList });
  });

  app.get("/host", (req, res) => {
    const hostname = os.hostname();
    res.status(200).json({ hostname });
  });

  app.post("/counter", (req, res) => {
    const { increment } = req.body;
    client.get("counter", (err, reply) => {
      if (err) {
        res.status(500).json({ err });
      }

      const parsedIncrement = parseInt(increment, 10);
      if (!reply) {
        client.set("counter", parsedIncrement);
        return res.status(200).json({ old: 0, new: parsedIncrement });
      }
      const newNumber = parseInt(reply.toString(), 10) + parsedIncrement;
      client.set("counter", `${newNumber}`);
      return res
        .status(200)
        .json({ old: parseInt(reply.toString()), new: newNumber });
    });
  });

  app.get("/counter", (req, res) => {
    client.get("counter", (err, reply) => {
      if (err) {
        res.status(500).json({ err });
      }
      return res.status(200).json({ counter: reply ? reply.toString() : "0" });
    });
  });

  const PORT = process.env.PORT || 8080;
  app.listen(PORT, function() {
    console.log(`Server is listenning on port ${PORT}`);
  });
});
