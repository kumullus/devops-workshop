#!/usr/bin/env bash
docker run --rm -d --name=workshop-redis redis

docker run --rm -d -e "REDIS_HOST=workshop-redis" -e "REDIS_PORT=6379" --name=workshop-api -p 8080:8080 workshop-backend:latest

docker run --rm -d -e "REACT_APP_API=http://localhost:8080" --name=workshop-client -p 3000:3000 workshop-frontend:latest

docker network connect --alias workshop-redis workshop-network workshop-redis
docker network connect --alias workshop-api workshop-network workshop-api
docker network connect --alias workshop-client workshop-network workshop-client
